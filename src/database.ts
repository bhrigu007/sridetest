
const url = 'mongodb://localhost:27017';
const dbName = 'pocathonserver';
import { MongoClient, Db } from 'mongodb';

export const database: any = {
    collections: {}
}

export function connectDatabase() {
    return MongoClient.connect(url, { useNewUrlParser: true })
        .then((connection: MongoClient) => {
            const dbConnection = connection.db(dbName);
            return createCollection(dbConnection);
        })
        .catch(err => {
            console.log(`Error connecting to mongodb: ${err}`);
            Promise.reject(err);
        });
};

function createCollection(connection: Db) {
    database.collections = {
        Audit: connection.collection('Audit')
    };
};

export function insertOne(collection: any, data: any) {
    return collection.insertOne(data);
}