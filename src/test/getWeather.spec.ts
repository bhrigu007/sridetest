const chai = require('chai');
const sinon = require('sinon');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised).should();

import { getResponse, isTodayDayPrime, addAudit } from '../controller/getWeather';
import * as db from '../database';

describe('getWeather Controller', () => {

    describe('isTodayDayPrime', function () {
        let sandbox: any;
        let clock: any;
        beforeEach(() => {
            sandbox = sinon.createSandbox();
        });

        afterEach(() => {
            sandbox.restore();
            clock.restore();
        });

        it('should return true if day is a prime number', function () {
            clock = sinon.useFakeTimers(new Date(2019, 1, 5).getTime());
            return isTodayDayPrime().should.eventually.equal(true);
        });

        it('should return false if day is not a prime number', function () {
            clock = sinon.useFakeTimers(new Date(2019, 1, 10).getTime());
            return isTodayDayPrime().should.eventually.equal(false);
        });
    });



    describe('getResponse', function () {

        it('should return "Date is not prime so no date"', function () {
            return getResponse(false).should.be.equal('Date is not prime so no date')
        });

        it('should return object', function () {
            const result = {
                "coord": {
                    "lon": 145.77,
                    "lat": -16.92
                },
                "weather": [
                    {
                        "id": 802,
                        "main": "Clouds",
                        "description": "scattered clouds",
                        "icon": "03n"
                    }
                ],
                "base": "stations",
                "main": {
                    "temp": 300.15,
                    "pressure": 1007,
                    "humidity": 74,
                    "temp_min": 300.15,
                    "temp_max": 300.15
                },
                "visibility": 10000,
                "wind": {
                    "speed": 3.6,
                    "deg": 160
                },
                "clouds": {
                    "all": 40
                },
                "dt": 1485790200,
                "sys": {
                    "type": 1,
                    "id": 8166,
                    "message": 0.2064,
                    "country": "AU",
                    "sunrise": 1485720272,
                    "sunset": 1485766550
                },
                "id": 2172797,
                "name": "Cairns",
                "cod": 200
            };
            return getResponse(true).should.be.deep.equals(result);
        });
    });


    describe('addAudit', function () {
        afterEach(() => {
            sinon.restore();
        });

        it('should be fullfilled', function () {
            sinon.stub(db, 'insertOne').resolves(true);
            return addAudit('testing').should.eventually.fulfilled;
        });
    });
});
