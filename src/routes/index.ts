import { Router } from 'express';
import { getWeatherController } from '../controller/getWeather';

// API Server Endpoints
export const routes = (router: Router) => {
    router.get('/getWeather', getWeatherController);
};

