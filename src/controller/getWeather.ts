import { Response, Request } from "express";
import { database, insertOne } from '../database';

// json output from https://openweathermap.org/api
const responseObject = {
    "coord": {
        "lon": 145.77,
        "lat": -16.92
    },
    "weather": [
        {
            "id": 802,
            "main": "Clouds",
            "description": "scattered clouds",
            "icon": "03n"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 300.15,
        "pressure": 1007,
        "humidity": 74,
        "temp_min": 300.15,
        "temp_max": 300.15
    },
    "visibility": 10000,
    "wind": {
        "speed": 3.6,
        "deg": 160
    },
    "clouds": {
        "all": 40
    },
    "dt": 1485790200,
    "sys": {
        "type": 1,
        "id": 8166,
        "message": 0.2064,
        "country": "AU",
        "sunrise": 1485720272,
        "sunset": 1485766550
    },
    "id": 2172797,
    "name": "Cairns",
    "cod": 200
};


/**
 * Returns true is today date is prime number else false
 * @method isTodayDayPrime
 */
export function isTodayDayPrime(): Promise<boolean> {
    const day = new Date().getDate();
    let isPrime = true;
    for (let i = 2; i <= day / 2; i++) {
        if (day % i === 0) {
            isPrime = false;
            break;
        }
    }
    return Promise.resolve(isPrime);
}

/**
 * Add audit log for each request
 * @method addAudit
 * @param result result/response retured by api
 */
export function addAudit(result: Object | string) {
    const data = {
        result: result,
        date: new Date(),
    }
    return insertOne(database.collections.Audit, data);
}

/**
 * Returns json object is isPrime= true else returns 'Date is not prime so no date'
 * @method getResponse
 * @param isPrime 
 */
export function getResponse(isPrime: boolean): Object | string {
    return isPrime ? responseObject : 'Date is not prime so no date'
}


export const getWeatherController = (req: Request, res: Response) => {
    let response: Object | string;

    isTodayDayPrime()
        .then((isPrime: boolean) => {
            return getResponse(isPrime);
        })
        .then((data) => {
            response = data;
            return addAudit(response);
        })
        .then(() => {
            res.send(response);
        })
        .catch((err: any) => {
            console.log(`Error in getWeather ${JSON.stringify(err)}`);
            res.send('Something Went Wrong');
        });
};