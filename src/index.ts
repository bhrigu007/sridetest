import express from 'express';
import * as bodyParser from 'body-parser';
import { Router } from 'express';
import { connectDatabase } from './database';
const helmet = require('helmet');
const cors = require('cors');

// Create Express server
const app = express();
const router: Router = express.Router();

app.use(cors());
app.use(helmet());
app.set('port', process.env.PORT || 3000);
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
require('./routes/index').routes(router);
app.use(router);

connectDatabase()
    .then(() => {
        app.listen(app.get('port'), function () {
            console.log(`App running on port ${app.get('port')}`);
            console.log(' Press CTRL-C to stop\n');
        });
    })
    .catch((error: Error) => {
        console.log(`Existing/Terminating process`);
        process.exit(0);
    });